This module allows views to query the nodes referenced by a certain node reference field.
You can use it to filter results from a node reference, and show referenced nodes with pager. 


Dependencies:
* Nodereference module in the CCK package(http://drupal.org/project/cck).
* Views module (http://drupal.org/project/views).

Common usage example:
1. Enable the module.
2. Create a content type called Photo.
3. Create a conent type called Album with a node reference field named Photo reference, which references to Photos content type.
4. Create a new view, set the page URL to 'views_on_node_reference'. 
5. Add Argument 'Node Reference: Views on node reference'. 
6. Select the 'photo_reference' field .
7. Set filters such as 'node published'.
8. Set sort critireia such as 'sort descending'.
9. Save the view
10. Enter the view URL (i.e example.com/views_on_node_reference/nid - Where nid represents a the node is of an Album content.

The expected result is displaying, filtering and sorting of the referenced nodes. 


